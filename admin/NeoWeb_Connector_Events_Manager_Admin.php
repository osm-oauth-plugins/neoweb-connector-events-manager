<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    NeoWeb_Connector_Events_Manager
 * @subpackage NeoWeb_Connector_Events_Manager/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    NeoWeb_Connector_Events_Manager
 * @subpackage NeoWeb_Connector_Events_Manager/admin
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class NeoWeb_Connector_Events_Manager_Admin {

    private NeoWeb_Connector_Events_Manager_Auth_Caller $oAuthCaller;
    private NeoWeb_Connector_Admin_Notifications $flashNotice;


	private $plugin_data;

		/**
		 * @param $key
		 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-events-manager');
		$this->oAuthCaller = new NeoWeb_Connector_Events_Manager_Auth_Caller();
        $this->flashNotice = new NeoWeb_Connector_Admin_Notifications();
    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NeoWeb_Connector_Events_Manager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NeoWeb_Connector_Events_Manager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'css/neoweb-connector-events-manager-admin.css', array(), $this->get_plugin_data('pluginVersion'), 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NeoWeb_Connector_Events_Manager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NeoWeb_Connector_Events_Manager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'js/neoweb-connector-events-manager-admin.js', array( 'jquery' ), $this->get_plugin_data('pluginVersion'), false );

	}

    public function trigger_publish_click_scripts() {
        $screen = get_current_screen();
	    if ( $screen->id == "toplevel_page_neoweb-connector-licences" ) {
		    update_field($this->get_plugin_data('productSlug') . '_saved', "Yes", 'option');
	    } else if ( $screen->id == "neoweb-connector-events-manager_page_neoweb-connector-events-manager_plugin_settings_page" ) {
		    $sectionData = $this->oAuthCaller->getAvailableSections();
		    $dataString = array();
		    if (is_array($sectionData) || is_object($sectionData)) {
			    foreach ($sectionData as $result) {
				    $section = $result["section_type"];
				    $sectionID = $result["section_id"];
				    if ($section != "waiting" && $section != "discount") {
				    	//Loop all sections and only add them if they were ticked for inclusion
				    	if (get_field('group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') == 1) {
						    array_push($dataString, $sectionID);
					    }
				    }
			    }
		    }

		    update_field('group_cal_generated_shortcode' . $this->get_plugin_data('pluginSlug'), '[OSM_Group_Calendar]'. json_encode($dataString) . '[/OSM_Group_Calendar]', 'option');

	    } else if ( $screen->id == "neoweb-connector_page_neoweb-connector-app-settings" ) {

            $osm_oauth_client_id = get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_client_id", "option");
            $osm_oauth_secret = get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_secret", "option");

            if ($osm_oauth_client_id && $osm_oauth_secret) {
                $authData = $this->oAuthCaller->osm_authenticate();

                if ($authData['status'] == 'success') {
                    $this->flashNotice->add_flash_notice("Authentication with OSM application completed successfully", "success", false);
                } else {
                    $this->flashNotice->add_flash_notice("Something went wrong, please check the message below:<br/>".$authData['message'], "error", false);
                }

            } else {

                $this->flashNotice->add_flash_notice("The Client ID and Secret has not been entered, 
			    authorisation to OSM has been aborted. Please complete the instruction on this page before trying again.", "error", false);

            }
        }
    }

}
