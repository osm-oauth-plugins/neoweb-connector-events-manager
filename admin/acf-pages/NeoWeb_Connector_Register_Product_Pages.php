<?php


class NeoWeb_Connector_Register_Product_Pages {

    private NeoWeb_Connector_Loggers $logger;

    private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}
    
	private string $img_folder_path;

	/**
	 * @var string[]
	 */
	private array $productPages;

	/**
	 * @var string[]
	 */
	private array $protectedProductPages;

	/**
	 * Initialize class
	 *
	 */
	public function __construct()
	{
		$this->plugin_data = get_option('neoweb-connector-events-manager');
		$this->img_folder_path = plugin_dir_url( dirname(__FILE__) );

		$this->productPages = array(
			"licences" => "Product Licence(s)",
			"support" => "Support Settings"
		);

		$this->protectedProductPages = array(
			"app-settings" => "OSM Application Settings",
			"resources" => "Available OSM Resources",
			"debug-cache" => "Debug & Cache Settings",
		);

    }

    public function registerProductPages() {

	    if( function_exists('acf_add_options_page') ):

		    acf_add_options_page(array(
			    'page_title' => $this->get_plugin_data('productName'),
			    'menu_title' => $this->get_plugin_data('productName'),
			    'menu_slug' => $this->get_plugin_data('productSlug') . '_parent',
			    'capability' => 'manage_options',
			    'position' => '',
			    'parent_slug' => '',
			    'icon_url' => $this->img_folder_path . '/images/product-icon.png',
			    'redirect' => true,
		    ));

	        //Load Product Pages using $this->productPages
	        foreach ($this->productPages as $pageSlug => $pageTitle) {
		        acf_add_options_page(array(
			        'page_title' => $this->get_plugin_data('productName') . ' - ' . $pageTitle,
			        'menu_title' => $pageTitle,
			        'menu_slug' => $this->get_plugin_data('productSlug') . "-" . $pageSlug,
			        'capability' => 'manage_options',
			        'position' => '999',
			        'parent_slug' => $this->get_plugin_data('productSlug') . '_parent',
			        'icon_url' => '',
			        'post_id' => 'options',
			        'autoload' => false,
			        'update_button' => 'Update',
			        'updated_message' => 'Details updated',
		        ));
	        }

	    endif;
	}

    public function registerProtectedProductPages() {

	    if( function_exists('acf_add_options_page') ):

	        //Load Product Pages using $this->productPages
	        foreach ($this->protectedProductPages as $pageSlug => $pageTitle) {
		        acf_add_options_page(array(
			        'page_title' => $this->get_plugin_data('productName') . ' - ' . $pageTitle,
			        'menu_title' => $pageTitle,
			        'menu_slug' => $this->get_plugin_data('productSlug') . "-" . $pageSlug,
			        'capability' => 'manage_options',
			        'position' => '999',
			        'parent_slug' => $this->get_plugin_data('productSlug') . '_parent',
			        'icon_url' => '',
			        'post_id' => 'options',
			        'autoload' => false,
			        'update_button' => 'Update',
			        'updated_message' => 'Details updated',
		        ));
	        }

	    endif;
	}

	public function registerProductPageLogos() {

		if( function_exists('acf_add_local_field_group') ):

			$locationArray = array();
			foreach ($this->productPages as $pageSlug => $pageTitle) {
				$productPage = array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => $this->get_plugin_data('productSlug') . "-" . $pageSlug,
					),
				);
				array_push($locationArray, $productPage);
			}

			//Load Product Pages Logos
			acf_add_local_field_group(array(
				'key' => 'group_product_logo',
				'title' => 'plugin_logo',
				'fields' => array(
					array(
						'key'     => 'field_product_logo',
						'label'   => '',
						'name'    => 'product_logo',
						'type'    => 'message',
						'message' => '<div class="logoWrapper">
							<img src="' . $this->img_folder_path . '/images/product-logo.png"></div>',
					)
				),
				'location' => $locationArray,
				'menu_order' => - 1,
				'position' => 'acf_after_title',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;

	}

	public function registerProtectedProductPageLogos() {

		if( function_exists('acf_add_local_field_group') ):

			$locationArray = array();
			foreach ($this->protectedProductPages as $pageSlug => $pageTitle) {
				$productPage = array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => $this->get_plugin_data('productSlug') . "-" . $pageSlug,
					),
				);
				array_push($locationArray, $productPage);
			}

			//Load Product Pages Logos
			acf_add_local_field_group(array(
				'key' => 'group_protected_product_logo',
				'title' => 'plugin_logo',
				'fields' => array(
					array(
						'key'     => 'field_protected_product_logo',
						'label'   => '',
						'name'    => 'protected_product_logo',
						'type'    => 'message',
						'message' => '<div class="logoWrapper">
							<img src="' . $this->img_folder_path . '/images/product-logo.png"></div>',
					)
				),
				'location' => $locationArray,
				'menu_order' => - 1,
				'position' => 'acf_after_title',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;

	}

}