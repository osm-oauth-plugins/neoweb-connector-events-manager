<?php


class NeoWeb_Connector_Events_Manager_Settings_Page
{

	protected NeoWeb_Connector_Loggers $logger;
	private $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 */
	public function __construct()
	{
		$this->plugin_data = get_option('neoweb-connector-statistics-manager');
		$this->pageID = $this->get_plugin_data('pluginSlug') . '_plugin_settings_page';
	}

    public function registerSettingPage() {
	    $img_folder_path = plugin_dir_url( dirname(__FILE__) );
        if( function_exists('acf_add_options_page') ):

	        acf_add_options_page(array(
		        'page_title' => $this->get_plugin_data('pluginName'),
		        'menu_title' => str_replace("NeoWeb Connector - ", "", $this->get_plugin_data('pluginName')),
		        'menu_slug' => $this->get_plugin_data('pluginSlug') . '_parent',
		        'capability' => 'manage_options',
		        'position' => '',
		        'parent_slug' => '',
		        'icon_url' => $img_folder_path . '/images/product-icon.png',
		        'redirect' => true,
	        ));

            acf_add_options_page(array(
                'page_title' => 'Shortcodes & Settings',
                'menu_title' => 'Plugin Settings',
                'menu_slug' => $this->pageID,
                'capability' => 'manage_options',
                'position' => '5',
                'parent_slug' => $this->get_plugin_data('pluginSlug') . '_parent',
                'icon_url' => '',
                'redirect' => false,
                'post_id' => 'options',
                'autoload' => false,
                'update_button' => 'Update',
                'updated_message' => 'Options Updated',
            ));

        endif;
    }

    public function registerPluginLogo() {
	    $img_folder_path = plugin_dir_url( dirname(__FILE__) );
	    if( function_exists('acf_add_local_field_group') ):
		    acf_add_local_field_group(array(
			    'key' => 'group_logo' . $this->pageID,
			    'title' => 'plugin_logo',
			    'fields' => array(
			    ),
			    'location' => array(
				    array(
					    array(
						    'param' => 'options_page',
						    'operator' => '==',
						    'value' => $this->pageID,
					    ),
				    ),
			    ),
			    'menu_order' => -1,
			    'position' => 'acf_after_title',
			    'style' => 'seamless',
			    'label_placement' => 'top',
			    'instruction_placement' => 'field',
			    'hide_on_screen' => '',
			    'active' => true,
			    'description' => '',
		    ));

		    acf_add_local_field(array(
			    'key' => 'field_' . 'logo_' . $this->pageID,
			    'label' => '',
			    'name' => 'logo_' . $this->pageID,
			    'type' => 'message',
			    'message' => '<div class="logoWrapper">
        <img src="' . $img_folder_path . '/images/logo.png"></div>',
			    'parent' => 'group_logo' . $this->pageID,
		    ));
	    endif;
    }

    public function registerSettingsPageNotices() {
	    if( function_exists('acf_add_local_field_group') ):
		    acf_add_local_field_group(array(
			    'key' => 'group_cal_notices',
			    'title' => 'group_cal_notices',
			    'fields' => array(
			    ),
			    'location' => array(
				    array(
					    array(
						    'param' => 'options_page',
						    'operator' => '==',
						    'value' => $this->pageID,
					    ),
				    ),
			    ),
			    'menu_order' => 1,
			    'position' => 'acf_after_title',
			    'style' => 'seamless',
			    'label_placement' => 'top',
			    'instruction_placement' => 'field',
			    'hide_on_screen' => '',
			    'active' => true,
			    'description' => '',
		    ));

		    acf_add_local_field(array(
			    'key' => 'field_' . 'group_cal_fields_' . $this->get_plugin_data('pluginSlug'),
			    'label' => '',
			    'name' => 'field_' . 'group_cal_fields_' . $this->get_plugin_data('pluginSlug'),
			    'type' => 'message',
			    'message' => 'Please select which sections to include in the events calendar and then click "Update" to reveal/update the shortcode.',
			    'parent' => 'group_cal_notices',
			    'wrapper' => array (
				    'width' => '',
				    'class' => 'neowebNotice',
				    'id' => '',
			    ),
		    ));
	    endif;
    }

    public function registerSettingsPageFields($resourceData, $sectionsMetaData) {
	    if( function_exists('acf_add_local_field_group') ):

	        acf_add_local_field_group(array(
			    'key' => 'group_cal_available_sections',
			    'title' => 'Available Sections',
			    'fields' => array(
			    ),
			    'location' => array(
				    array(
					    array(
						    'param' => 'options_page',
						    'operator' => '==',
						    'value' => $this->pageID,
					    ),
				    ),
			    ),
			    'menu_order' => 10,
			    'position' => 'normal',
			    'style' => 'default',
			    'label_placement' => 'top',
			    'instruction_placement' => 'field',
			    'hide_on_screen' => '',
			    'active' => true,
			    'description' => '',
		    ));

		    acf_add_local_field(array(
			    'key' => 'field_' . 'group_cal_refresh_fields_' . $this->get_plugin_data('pluginSlug'),
			    'label' => 'Data on this page is refreshed every 24 hours.<br/>Time left until next refresh',
			    'name' => 'field_' . 'group_cal_refresh_fields_' . $this->get_plugin_data('pluginSlug'),
			    'type' => 'message',
			    'message' =>  $sectionsMetaData,
			    'parent' => 'group_cal_available_sections',
		    ));

		    foreach ($resourceData as $groupID => $groupSections) {
			    acf_add_local_field(array(
				    'key' => 'field_' . 'group_cal_fields_accordion' . $this->get_plugin_data('pluginSlug') . '_' . $groupID,
				    'label' => $groupSections[0]['group_name'] . ' (Group ID: ' . $groupID . ')',
				    'name' => 'field_' . 'group_cal_fields_accordion' . $this->get_plugin_data('pluginSlug') . '_' . $groupID,
				    'type' => 'accordion',
				    'parent' => 'group_cal_available_sections',
			    ));
			    foreach ( $groupSections as $groupSection ) {
				    acf_add_local_field(array(
					    'key' => 'field_' . 'group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'label' => 'Include this section',
					    'name' => 'field_' . 'group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'type' => 'true_false',
					    'message' => $groupSection['group_name'] . ' - ' . $groupSection['section_name'],
					    'parent' => 'group_cal_available_sections',
					    'wrapper' => array (
						    'width' => '',
						    'class' => 'sectionHeading',
						    'id' => '',
					    ),
				    ));
				    acf_add_local_field(array(
					    'key' => 'field_' . 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'label' => '',
					    'name' => 'field_' . 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'type' => 'true_false',
					    'message' => 'Show a key for this section',
					    'parent' => 'group_cal_available_sections',
					    'conditional_logic' => array(
						    array(
							    array(
								    'field' => 'group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
								    'operator' => '==',
								    'value' => 1
							    ),
						    ),
					    ),
				    ));
				    acf_add_local_field(array(
					    'key' => 'field_' . 'group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'label' => 'Events',
					    'name' => 'group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'type' => 'color_picker',
					    'parent' => 'group_cal_available_sections',
					    'instructions' => 'Background Colour',
					    'conditional_logic' => array(
						    array(
							    array(
								    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
								    'operator' => '==',
								    'value' => 1
							    ),
						    ),
					    ),
				    ));
				    acf_add_local_field(array(
					    'key' => 'field_' . 'group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'label' => '',
					    'name' => 'group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'type' => 'color_picker',
					    'parent' => 'group_cal_available_sections',
					    'instructions' => 'Text Colour',
					    'conditional_logic' => array(
						    array(
							    array(
								    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
								    'operator' => '==',
								    'value' => 1
							    ),
						    ),
					    ),
				    ));
				    acf_add_local_field(array(
					    'key' => 'field_' . 'group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'label' => 'Meetings / Programme',
					    'name' => 'group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'type' => 'color_picker',
					    'parent' => 'group_cal_available_sections',
					    'instructions' => 'Background Colour',
					    'conditional_logic' => array(
						    array(
							    array(
								    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
								    'operator' => '==',
								    'value' => 1
							    ),
						    ),
					    ),
				    ));
				    acf_add_local_field(array(
					    'key' => 'field_' . 'group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'label' => '',
					    'name' => 'group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
					    'type' => 'color_picker',
					    'parent' => 'group_cal_available_sections',
					    'instructions' => 'Text Colour',
					    'conditional_logic' => array(
						    array(
							    array(
								    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
								    'operator' => '==',
								    'value' => 1
							    ),
						    ),
					    ),
				    ));

			    }
		    }


	    endif;

    }

	public function registerSettingsPageShortCodes() {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_cal_shortcode',
				'title' => 'Event & Program Calendar Shortcode',
				'fields' => array(
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 5,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field(array(
				'key' => 'field_' . 'group_cal_generated_shortcode' . $this->get_plugin_data('pluginSlug'),
				'label' => '',
				'name' => 'group_cal_generated_shortcode' . $this->get_plugin_data('pluginSlug'),
				'type' => 'text',
				'wrapper' => array(
					'class' => 'shortCodeCopy',
				),
				'readonly' => 1,
				'default_value' => '[OSM_Group_Calendar sections=\'NO SECTIONS SELECTED\']',
				'parent' => 'group_cal_shortcode',
			));
		endif;
	}

}