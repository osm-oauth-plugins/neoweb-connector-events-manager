<?php


class NeoWeb_Connector_Register_Debug_Cache_Page {

	private string $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 *
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-events-manager');
		$this->pageID = $this->get_plugin_data('productSlug') . '-debug-cache';
	}

	public function registerDebugSettingsPageDebugFields () {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('pluginSlug') . '_debugSettings_Group',
				'title' => $this->get_plugin_data('pluginName') . '- Debug and Cache Settings',
				'fields' => array(
					array(
						'key' => $this->get_plugin_data('pluginSlug') . 'caution_notice',
						'label' => '',
						'name' => $this->get_plugin_data('pluginSlug') . 'caution_notice',
						'type' => 'message',
						'message' => '<p class="neoweb_osm_oauth_connector_notice">Please Note: These options should only be activated if instructed to do so by NeoWeb staff, as these settings will have an impact on the performance of your website.</p>',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_debug_logs',
						'label' => 'Debug Logs',
						'name' => $this->get_plugin_data('pluginSlug') . 'debug_logs',
						'type' => 'tab',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_enable_debug_logs',
						'label' => '',
						'name' => $this->get_plugin_data('pluginSlug') . '_enable_debug_logs',
						'type' => 'true_false',
						'message' => 'Enable extensive debug logging',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_clear_logs',
						'label' => 'Clear extensive debug logs',
						'name' => $this->get_plugin_data('pluginSlug') . '_clear_logs',
						'type' => 'message',
						'message' => '<button type="button" class="button-secondary" id="'. str_replace("-", "_", $this->get_plugin_data('pluginSlug')) . 'triggerLogRefresh">Clear Debug Logs</button>',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_enable_api_call_logs',
						'label' => '',
						'name' => $this->get_plugin_data('pluginSlug') . '_enable_api_call_logs',
						'type' => 'true_false',
						'instructions' => 'This setting will record all API calls made from this plugin along with the data we received from OSM.',
						'message' => 'Enable API Call Logging',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_clear_api_logs',
						'label' => 'Clear API Call logs',
						'name' => $this->get_plugin_data('pluginSlug') . '_clear_api_logs',
						'type' => 'message',
						'message' => '<button type="button" class="button-secondary" id="'. str_replace("-", "_", $this->get_plugin_data('pluginSlug')) . 'triggerTransientLogRefresh">Clear Transient Debug Logs</button>',

					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_cacheTab',
						'label' => 'Cache',
						'name' => $this->get_plugin_data('pluginSlug') . '_cacheTab',
						'type' => 'tab',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_clear_wp_cache',
						'label' => 'Clear WordPress Plugin Cache',
						'name' => $this->get_plugin_data('pluginSlug') . '_clear_wp_cache',
						'type' => 'message',
						'message' => '<button type="button" class="button-secondary" id="'. str_replace("-", "_", $this->get_plugin_data('pluginSlug')) . 'triggerCacheRefresh">Clear Transient Cache</button>',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . 'cache_refresh_notice',
						'label' => '',
						'name' => $this->get_plugin_data('pluginSlug') . 'cache_refresh_notice',
						'type' => 'message',
						'message' => '<p class="neoweb_osm_oauth_connector_notice">Please Note: Data refresh requests from OSM are limited per hour, clicking refresh too often will cause the plugin to stop working for an hour</p>',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 10,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;
	}
}