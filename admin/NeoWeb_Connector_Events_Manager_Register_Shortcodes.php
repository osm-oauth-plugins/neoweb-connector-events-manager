<?php


class NeoWeb_Connector_Events_Manager_Register_Shortcodes {

	private NeoWeb_Connector_Events_Manager_Auth_Caller $oAuthCaller;
	private NeoWeb_Connector_Loggers $logger;

	/**
	 * @var false|mixed|void
	 */
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __contructor
	 *
	 * @since    1.0.0
	 */
	public function __construct( ) {
		$this->plugin_data = get_option('neoweb-connector-events-manager');
		$this->oAuthCaller = new NeoWeb_Connector_Events_Manager_Auth_Caller();
	}

	public function fetch_group_event_program_data ($attr, $sectionIDs): string {

		$eventsArray = array();
		foreach (json_decode($sectionIDs) as $sectionID) {

			$currentTermID = $this->oAuthCaller->getCurrentTermID($sectionID);

			if ($currentTermID != "") {
				$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getAllEventData;
				$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID);

				$transientID = 'event_data_' . $sectionID;
				$eventsData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

				foreach ($eventsData['items'] as $event) {
					$startDate = $event['startdate'];
					$formatStartDate = str_replace('/', '-', $startDate);

					$endDate = $event['enddate'];
					$formatEndDate = str_replace('/', '-', $endDate);

					$eventData = array(
						'id'                => $event['eventid'],
						'title'             => $event['name'],
						'start'             => date('Y-m-d', strtotime($formatStartDate)) . " " . $event['starttime'],
						'end'               => date('Y-m-d', strtotime($formatEndDate)) . " " . $event['endtime'],
						'backgroundColor'   => (get_field('group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#7b9b21"),
	                    'textColor'         => (get_field('group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#FFFFFF"),
					);
					array_push($eventsArray, $eventData);
				}

				$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getAllProgramData;
				$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID);

				$transientID = 'program_data_' . $sectionID;
				$programData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

				foreach ($programData['items'] as $meeting) {

					$meetingData = array(
						'id'                => $meeting['eveningid'],
						'title'             => $meeting['title'],
						'start'             => $meeting['meetingdate'] . " " . $meeting['starttime'],
						'end'               => $meeting['meetingdate'] . " " . $meeting['endtime'],
						'backgroundColor'   => (get_field('group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#6b17d5"),
						'textColor'         => (get_field('group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#FFFFFF"),
					);
					array_push($eventsArray, $meetingData);
				}
			}
		}

		$calData = json_encode($eventsArray);
		$pluginSlug = $this->get_plugin_data('pluginSlug');
		$html = ob_get_clean();
		ob_start();
		include(OSM_EVENTS_PLUGIN_PATH . 'public/html-templates/calendar.php' );
		$html .= ob_get_clean();

		return $html;

	}


}