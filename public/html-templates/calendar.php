<style>
    /*word wrap on group calendar and colours on the key*/
    .fc-title {
        padding: 0 1px;
        white-space: normal;
    }

    .key{
        margin-top: 15px;
    }
    .keyevents  {
        background-color:#7b9b21;
        color:#ffffff;
        padding: 5px;
        margin-right: 10px;
    }
    .keymeetings  {
        background-color:#6b17d5;
        color:#ffffff;
        padding: 5px;
    }
</style>

<div id="NeoWeb_fullcalendar"></div>


<?php foreach (json_decode($sectionIDs) as $sectionID) {

    if (get_field('show_key_' . $pluginSlug . "_" . $sectionID, 'option')) { ?>

    <div class="key">
        <? echo $this->oAuthCaller->getGroupName($sectionID); ?> -
        <? echo $this->oAuthCaller->getSectionName($sectionID); ?> Key:
            <span class="keyevents"
                  style="background-color: <? echo get_field('group_cal_events_key_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;
                          color: <? echo get_field('group_cal_events_text_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;">
                Events
            </span>
            <span class="keymeetings"
                  style="background-color: <? echo get_field('group_cal_meetings_key_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;
                    color: <? echo get_field('group_cal_meetings_text_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;">
                Programme meetings
            </span>
    </div>
<?php }
}?>


<script>

    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('NeoWeb_fullcalendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            events: <?php echo $calData; ?>
        });
        calendar.render();
    });

</script>