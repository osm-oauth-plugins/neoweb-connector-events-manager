<?php

/**
 * Fired during plugin activation
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    NeoWeb_Connector_Events_Manager
 * @subpackage NeoWeb_Connector_Events_Manager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    NeoWeb_Connector_Events_Manager
 * @subpackage NeoWeb_Connector_Events_Manager/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class NeoWeb_Connector_Events_Manager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		if ( !function_exists( 'get_plugins' ) ){
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		$plugin_data = get_option('neoweb-connector-events-manager');

		$headers = "Content-Type: text/html; charset=UTF-8" .PHP_EOL;

		$to = $plugin_data['supportEmail'];
		$subject = "New Site Registered/Activated:" . $plugin_data['pluginName'];

		ob_start();

		include(plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/register-plugin-email-template.php');
		$htmlMessage = ob_get_clean();

		wp_mail( $to, $subject, $htmlMessage, $headers);

	}

}
