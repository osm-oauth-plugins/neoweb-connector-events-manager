<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    NeoWeb_Connector_Events_Manager
 * @subpackage NeoWeb_Connector_Events_Manager/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    NeoWeb_Connector_Events_Manager
 * @subpackage NeoWeb_Connector_Events_Manager/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class NeoWeb_Connector_Events_Manager_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		$pluginData = get_option('neoweb-connector-events-manager');

		//Delete the contents of the osmDebug folder
		$path = plugin_dir_path( dirname( __FILE__ ) );
		$logger = new NeoWeb_Connector_Loggers(
			plugin_dir_path( dirname( __FILE__ ) )
		);

		$logger->recursiveRemove($path . 'logs');

		//Disable the osmDebug option if active
		update_field($pluginData['pluginSlug'] . '_enable_api_call_logs', 0, 'option');
		update_field($pluginData['pluginSlug'] . '_enable_debug_logs', 0, 'option');

		$transientManager = new NeoWeb_Connector_Events_Manager_Transient_Manager(
			$pluginData['pluginSlug'] . "_osm"
		);

		//Delete all transients
		$transientManager->wds_delete_transients();
	}

}
